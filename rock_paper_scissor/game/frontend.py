from rock_paper_scissor.game.choice import Winners
from rock_paper_scissor.game.validator import is_valid_gamemode, is_valid_user_input


def select_game_mode():
    print('Welcome to RockPaperScissor')
    print('Please choose a gamemode:')
    print('\t1 Randomizer AI')
    print('\t2 AI Mode')

    selected_game_mode = input('Gamemode: ')

    if is_valid_gamemode(selected_game_mode):
        print('You chose "{}"'.format(selected_game_mode))
        launch_game(int(selected_game_mode))
    else:
        print('Please select a correct gamemode.')
        select_game_mode()


def launch_game(game_mode):
    from rock_paper_scissor.game.game import Game
    game = Game(game_mode)

    playing = True
    while playing:
        game.start()

        playing = game.playing
        if playing:
            print('You chose "{}"'.format(game.user_input.value))
            print('The computer chose "{}"'.format(game.attack.value))

            if game.winner == Winners.PLAYER:
                print('You won!')
            elif game.winner == Winners.COMPUTER:
                print('You lost!')
            elif game.winner == Winners.TIE:
                print('It was a tie!')
            else:
                print('Something went wrong, sorry!')
        else:
            print('Goodbye!')


def ask_user_input():
    selected_input = input('Enter your choice [rock, paper, scissor, quit]: ')

    if is_valid_user_input(selected_input):
        return selected_input
    else:
        print('Please select a correct input.')
        selected_input = ask_user_input()

    return selected_input
