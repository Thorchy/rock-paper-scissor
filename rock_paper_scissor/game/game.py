import random

from rock_paper_scissor.game.choice import Choice, GameMode, Winners
from rock_paper_scissor.game.frontend import ask_user_input


class Game:

    game_mode = None
    user_inputs = []

    user_input = None
    attack = None
    winner = None

    playing = True

    def __init__(self, game_mode):
        self.game_mode = GameMode(game_mode)

    def start(self):
        self.take_user_input()
        self.choose_attack()
        self._declare_winner()

    def take_user_input(self):
        self.user_input = Choice(ask_user_input())
        self.user_inputs.append(self.user_input)

        if self.user_input == Choice.QUIT:
            self.playing = False

    def choose_attack(self):
        if self.game_mode == GameMode.AI:
            self.attack = self._ai_attack()
        elif self.game_mode == GameMode.RANDOM:
            self.attack = self._ai_attack()

    def _random_attack(self):

        choice = Choice.QUIT

        while choice == Choice.QUIT:
            choice = random.choice(list(Choice))

        return choice

    def _ai_attack(self):
        if len(self.user_inputs) <= 1:
            return self._random_attack()

        def most_common(lst):
            return max(set(lst), key=lst.count)

        most_common = most_common(self.user_inputs)

        if most_common == Choice.ROCK:
            return Choice.PAPER
        elif most_common == Choice.PAPER:
            return Choice.SCISSOR
        elif most_common == Choice.SCISSOR:
            return Choice.ROCK
        else:
            return self._random_attack()

    def _declare_winner(self):
        if self.user_input == Choice.ROCK and self.attack == Choice.PAPER:
            self.winner = Winners.COMPUTER
        elif self.user_input == Choice.ROCK and self.attack == Choice.SCISSOR:
            self.winner = Winners.PLAYER
        elif self.user_input == Choice.ROCK and self.attack == Choice.ROCK:
            self.winner = Winners.TIE
        elif self.user_input == Choice.PAPER and self.attack == Choice.PAPER:
            self.winner = Winners.TIE
        elif self.user_input == Choice.PAPER and self.attack == Choice.SCISSOR:
            self.winner = Winners.COMPUTER
        elif self.user_input == Choice.PAPER and self.attack == Choice.ROCK:
            self.winner = Winners.PLAYER
        elif self.user_input == Choice.SCISSOR and self.attack == Choice.PAPER:
            self.winner = Winners.PLAYER
        elif self.user_input == Choice.SCISSOR and self.attack == Choice.SCISSOR:
            self.winner = Winners.TIE
        elif self.user_input == Choice.SCISSOR and self.attack == Choice.ROCK:
            self.winner = Winners.COMPUTER
