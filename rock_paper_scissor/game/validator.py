from rock_paper_scissor.game.choice import Choice, GameMode


def is_valid_gamemode(selected_gamemode):
    valid = False
    try:
        selected_gamemode = int(selected_gamemode)

        if selected_gamemode in [mode.value for mode in GameMode]:
            valid = True
    except ValueError:
        valid = False

    return valid


def is_valid_user_input(user_input):
    valid = False

    try:
        user_input = user_input.lower()

        if user_input in [choice.value for choice in Choice]:
            valid = True
    except ValueError:
        valid = False
    except AttributeError:
        valid = False

    return valid
