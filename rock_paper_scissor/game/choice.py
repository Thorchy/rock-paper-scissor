from enum import Enum, unique


@unique
class Choice(Enum):
    ROCK = 'rock'
    PAPER = 'paper'
    SCISSOR = 'scissor'
    QUIT = 'quit'


@unique
class GameMode(Enum):
    AI = 1
    RANDOM = 2


@unique
class Winners(Enum):
    PLAYER = 1
    COMPUTER = 2
    TIE = 3
