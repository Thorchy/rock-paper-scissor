import unittest

from rock_paper_scissor.game.validator import is_valid_user_input


class TestChoices(unittest.TestCase):

    def test_valid_choice_rock(self):
        self.assertEqual(is_valid_user_input('rock'), True)

    def test_valid_choice_paper(self):
        self.assertEqual(is_valid_user_input('paper'), True)

    def test_valid_choice_scissor(self):
        self.assertEqual(is_valid_user_input('scissor'), True)

    def test_valid_choice_quit(self):
        self.assertEqual(is_valid_user_input('quit'), True)

    def test_invalid_choice_integer(self):
        self.assertEqual(is_valid_user_input(1), False)

    def test_invalid_choice_string(self):
        self.assertEqual(is_valid_user_input('hello'), False)
