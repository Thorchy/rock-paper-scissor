import unittest

from rock_paper_scissor.game.validator import is_valid_gamemode


class TestGameMode(unittest.TestCase):

    def setUp(self):
        pass

    def test_correct_game_mode_1(self):
        self.assertEqual(is_valid_gamemode(1), True)

    def test_correct_game_mode_2(self):
        self.assertEqual(is_valid_gamemode(2), True)

    def test_incorrect_game_mode_string(self):
        self.assertEqual(is_valid_gamemode('Game Mode X'), False)

    def test_incorrect_game_mode_int(self):
        self.assertEqual(is_valid_gamemode(500), False)
